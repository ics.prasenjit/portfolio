import Head from "next/head";
import { Montserrat, Lato } from "@next/font/google";
import Navbar from "@/components/Navbar";
import HomeMain from "@/components/Home";
import ScrollToTop from "@/components/ScrollToTop";

import Footer from "@/components/Footer";
import Services from "@/components/Services";
import Skills from "@/components/Skills";
import Projects from "@/components/Projects";
import styles from "../styles/index.module.scss";
import { useState } from "react";

const montserrat = Montserrat({ variable: "--montserrat-font" });

export default function Home() {
  const [isDark, setIsDark] = useState(true);
  const themeStyles = {
    "--bg": isDark ? "rgb(33,39,47)" : "#f7f7f8",
    "--bg-dark": isDark ? "rgb(23,28,35)" : "#fff",
    "--text": isDark ? "#fff" : "#000",
    "--shadow": isDark
      ? "rgb(17 17 26 / 10%) 0px 8px 24px, rgb(17 17 26 / 10%) 0px 16px 56px, rgb(17 17 26 / 10%) 0px 24px 80px"
      : "0px 1px 2px 0px rgb(35,189,149,0.3), 1px 2px 4px 0px rgb(35,189,149,0.3), 2px 4px 8px 0px rgb(35,189,149,0.3), 2px 4px 16px 0px rgb(35,189,149,0.3)",
  };

  return (
    <>
      <Head>
        <title>Portfolio</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div style={themeStyles}>
        <main className={montserrat.variable}>
          <div className={styles.main_container}>
            <Navbar isDark={isDark} setIsDark={setIsDark} />
            <HomeMain />
            <Services />
            <Skills />
            <Projects />
            <Footer />
            <ScrollToTop />
          </div>
        </main>
      </div>
    </>
  );
}
