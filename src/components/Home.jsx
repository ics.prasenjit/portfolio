import React from "react";
import styles from "./../styles/Home.module.scss";
import DownloadIcon from "@mui/icons-material/Download";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import InstagramIcon from "@mui/icons-material/Instagram";
import FacebookIcon from "@mui/icons-material/Facebook";
import Image from "next/image";
import Polygon from "./../assets/images/polygon.png";
import MyImg from "./../assets/images/img.png";

const Home = () => {
  return (
    <div className={styles.main_container}>
      <div className={styles.container_1}>
        <div className={styles.sub_container_1}>
          <h4>
            Hello, <span>I'm</span>
          </h4>
          <h3>Prasenjit Chakraborty</h3>
          <h4>Full Stack Web Developer</h4>
          <p>With proficiency in MERN stack</p>
          <br />
          <button>
            <a
              href=""
              download
              target="_blank"
            >
              <DownloadIcon />
              Resume
            </a>
          </button>
        </div>
        <div className={styles.sub_container_2}>
          <p>Check Out My</p>
          <div className={styles.social_icons}>
            <a href="https://www.linkedin.com/in/prasenjit-chakraborty-a3127a1b2" target="_blank">
              <LinkedInIcon className={styles.social_icon} />
            </a>
            <a href="https://www.instagram.com/prasenjitbob" target="_blank">
              <InstagramIcon className={styles.social_icon} />
            </a>
            <a href="https://www.facebook.com/prasenjit.chakraborty.902" target="_blank">
              <FacebookIcon className={styles.social_icon} />
            </a>
          </div>
        </div>
      </div>
      <div className={styles.img_container}>
        <Image className={styles.polygon} src={Polygon} width={500} height={387} />
        <Image src={MyImg} className={styles.my_img} />
      </div>
    </div>
  );
};

export default Home;
