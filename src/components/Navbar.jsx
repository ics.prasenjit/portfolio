import React, { useState } from "react";
import styles from "./../styles/Navbar.module.scss";
import CategoryIcon from "@mui/icons-material/Category";
import { Button, Drawer } from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import CustomizedSwitches from "./DarkModeBtn";
import { useRouter } from "next/router";

const Navbar = ({ isDark, setIsDark }) => {
  const router = useRouter();
  const [drawerIsOpen, setdrawerIsOpen] = useState(false);
  const pathHome = router.pathname === "/";

  const darkModeBtn = () => {
    return <CustomizedSwitches isDark={isDark} setIsDark={setIsDark} />;
  };
  const myStyle = {
    backgroundColor: isDark ? "rgb(23,28,35)" : "#fff",
    color: isDark ? "#fff" : "#000",
  };

  const scrollToProject = () => {
    return window.scrollTo({
      top: 2290,
      behavior: "smooth",
    });
  };

  const scrollToSkills = () => {
    return window.scrollTo({
      top: 1600,
      behavior: "smooth",
    });
  };

  const hamburgerMenu = () => {
    const handleDrawerScroll = (section) => {
      setdrawerIsOpen(false);
      window.scrollTo({
        top: section === "skills" ? 2150 : 2700,
        behavior: "smooth",
      });
    };
    return (
      <>
        <Button onClick={() => setdrawerIsOpen(true)}>
          <MenuIcon />
        </Button>
        <Drawer anchor="right" open={drawerIsOpen} onClose={() => setdrawerIsOpen(false)}>
          <div
            className={styles.drawer}
            style={{
              backgroundColor: isDark ? "rgb(33,39,47)" : "#f7f7f8",
              color: isDark ? "#fff" : "#000",
            }}
          >
            {darkModeBtn()}
            <div className={styles.drawer_nav_items}>
              <div style={{ ...myStyle, color: pathHome && "rgb(35,189,149)" }}>Home</div>
              <div style={myStyle} onClick={() => handleDrawerScroll("skills")}>
                Skills
              </div>
              <div style={myStyle} onClick={() => handleDrawerScroll("projects")}>Projects</div>
            </div>
          </div>
        </Drawer>
      </>
    );
  };
  return (
    <div className={styles.navbar}>
      <div className={styles.container_1}>
        <CategoryIcon className={styles.portfolio_icon} />
        Portfolio
      </div>
      <div className={styles.nav_items}>
        <div style={{ color: pathHome && "rgb(35,189,149)" }}>Home</div>
        <div onClick={scrollToSkills}>Skills</div>
        <div onClick={scrollToProject}>Projects</div>
      </div>
      <div className={styles.dark_btn}>{darkModeBtn()}</div>
      <div className={styles.hamburger_menu}>{hamburgerMenu()}</div>
    </div>
  );
};

export default Navbar;
