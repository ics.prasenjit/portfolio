import React from "react";
import LaptopChromebookIcon from "@mui/icons-material/LaptopChromebook";
import StorageIcon from "@mui/icons-material/Storage";
import HandymanIcon from "@mui/icons-material/Handyman";
import styles from "./../styles/Services.module.scss";

const Services = () => {
  return (
    <div className={styles.main_container}>
      <div className={styles.container}>
        <div className={styles.heading}>
          <h4>
            My <span>services</span>
          </h4>
          <h3>What I Do</h3>
        </div>
        <div className={styles.services}>
          <div className={styles.card}>
            <LaptopChromebookIcon className={styles.card_icon} />
            <h5>Frontend Development</h5>
            <p>Develop highly interactive Front end / User Interfaces for your web applications</p>
          </div>
          <div className={styles.card}>
            <StorageIcon className={styles.card_icon} />
            <h5>Backend Development</h5>
            <p>Creating application backend in Node, Express & MongoDB</p>
          </div>
          <div className={styles.card}>
            <HandymanIcon className={styles.card_icon} />
            <h5>Third Party Services</h5>
            <p>Integration of third party services such as Firebase, Chat Engine, etc.</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Services;
