import React from "react";
import styles from "./../styles/Footer.module.scss";
import FacebookIcon from "@mui/icons-material/Facebook";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import InstagramIcon from "@mui/icons-material/Instagram";
import HomeIcon from "@mui/icons-material/Home";
import LocalPhoneIcon from "@mui/icons-material/LocalPhone";
import EmailIcon from "@mui/icons-material/Email";

const Footer = () => {
  return (
    <div className={styles.main_container}>
      <div className={styles.container_1}>
        <h4>Portfolio</h4>
      </div>
      <div className={styles.container_3}>
        <p>Check Out My</p>
        <div className={styles.social_icons}>
          <a href="https://www.linkedin.com/in/prasenjit-chakraborty-a3127a1b2" target="_blank">
            <LinkedInIcon className={styles.social_icon} />
          </a>
          <a href="https://www.instagram.com/prasenjitbob" target='_blank'>
            <InstagramIcon className={styles.social_icon} />
          </a>
          <a href="https://www.facebook.com/prasenjit.chakraborty.902" target='_blank'>
            <FacebookIcon className={styles.social_icon} />
          </a>
        </div>
      </div>
      <div className={styles.container_2}>
        <h5>Contact Me</h5>
        <div className={styles.hr}></div>
        <div className={styles.contact}>
          <p className={styles.address}>
            <HomeIcon className={styles.icon} />
            <span>A-6, Vejalpur, Ahmedabad, Gujarat: 380051</span>
          </p>
          <p className={styles.phone}>
            <LocalPhoneIcon className={styles.icon} />
            <a href="tel:918752908711">+91-8752908711</a>
          </p>
          <p className={styles.email}>
            <EmailIcon className={styles.icon} />
            <a href="mailto:ics.prasenjit@gmail.com">ics.prasenjit@gmail.com</a>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Footer;
