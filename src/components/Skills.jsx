import React, { useState } from "react";
import styles from "./../styles/Skills.module.scss";
import ArrowCircleLeftIcon from "@mui/icons-material/ArrowCircleLeft";
import ArrowCircleRightIcon from "@mui/icons-material/ArrowCircleRight";
import ReactImg from "./../assets/images/react.png";
import NodeImg from "./../assets/images/node.png";
import MongoImg from "./../assets/images/mongo.png";
import ExpressImg from "./../assets/images/express.png";
import NextImg from "./../assets/images/next.png";
import ReduxImg from "./../assets/images/redux.png";
import TsImg from "./../assets/images/ts.png";
import FirebaseImg from "./../assets/images/firebase.png";
import GitlabImg from "./../assets/images/gitlab.png";
import Image from "next/image";

const Skills = () => {
  const skills = [
    {
      category: "Frontend",
      name: "Gitlab",
      img: GitlabImg,
    },
    {
      category: "Frontend",
      name: "Firebase",
      img: FirebaseImg,
    },
    {
      category: "Frontend",
      name: "ReactJs",
      img: ReactImg,
    },
    {
      category: "Frontend",
      name: "NodeJs",
      img: NodeImg,
    },
    {
      category: "Frontend",
      name: "Typescript",
      img: TsImg,
    },
    {
      category: "Frontend",
      name: "ExpressJs",
      img: ExpressImg,
    },
    {
      category: "Frontend",
      name: "Mongo DB",
      img: MongoImg,
    },
    {
      category: "Frontend",
      name: "Redux",
      img: ReduxImg,
    },
    {
      category: "Frontend",
      name: "NextJs",
      img: NextImg,
    },
 
  ];

  const [lastIndex, setLastIndex] = useState(4);
  const [skillSToShow, setSkillSToShow] = useState(skills.slice(0, 5));

  const nextSkill = () => {
    let tempSkillsArr = [...skillSToShow];
    if (lastIndex + 1 === skills.length) {
      tempSkillsArr.push(skills[0]);
      setLastIndex(0);
    } else {
      tempSkillsArr.push(skills[lastIndex + 1]);
      setLastIndex(lastIndex + 1);
    }

    tempSkillsArr.shift(0);
    setSkillSToShow(tempSkillsArr);
  };

  const prevSkill = () => {
    let tempSkillsArr = [...skillSToShow];

    if (lastIndex <= 4) {
      tempSkillsArr.unshift(skills[skills.length + lastIndex - 5]);
      lastIndex === 0 ? setLastIndex(skills.length - 1) : setLastIndex(lastIndex - 1);
    } else {
      tempSkillsArr.unshift(skills[lastIndex - 5]);
      setLastIndex(lastIndex - 1);
    }
    tempSkillsArr.pop(4);
    setSkillSToShow(tempSkillsArr);
  };

  return (
    <div className={styles.main_container}>
      <div className={styles.container}>
        <h3>
          My <span>Skills</span>
        </h3>
        <div className={styles.skills}>
          <div className={styles.skill_cards}>
            {skillSToShow?.length
              ? skillSToShow.map((skill, i) => (
                  <div key={i} className={styles.skill}>
                    <Image className={styles.skill_icon} src={skill.img} />
                    <p>{skill.name}</p>
                  </div>
                ))
              : null}
          </div>
          <div className={styles.buttons}>
            <div onClick={prevSkill}>
              <ArrowCircleLeftIcon className={styles.button_icon} />
            </div>
            <div onClick={nextSkill}>
              <ArrowCircleRightIcon className={styles.button_icon} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Skills;
