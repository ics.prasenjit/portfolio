import React from "react";
import styles from "./../styles/Projects.module.scss";
import bobTube from "./../assets/images/bobtube.png";
import bobMusic from "./../assets/images/bobmusic.png";
import bobChat from "./../assets/images/bobchat.png";
import Image from "next/image";
import Link from "next/link";

const Projects = () => {
  return (
    <div className={styles.main_container}>
      <div className={styles.container}>
        <div className={styles.heading}>
          <h3>
            Recent <span>Projects</span>
          </h3>
        </div>
        <div className={styles.projects}>
          <Link href="https://bobtube.vercel.app/" className={styles.card}>
            <div className={styles.project_img_container}>
              <Image className={styles.project_img} src={bobTube} />
            </div>
            <div className={styles.project_details}>
              <h5>
                BoB<span>Tube</span>
              </h5>
              <p>Video streaming Web application</p>
            </div>
          </Link>
          <Link href="https://bobmusic-gold.vercel.app/" className={styles.card}>
            <div className={styles.project_img_container}>
              <Image className={styles.project_img} src={bobMusic} />
            </div>
            <div className={styles.project_details}>
              <h5>
                BoB<span>Music</span>
              </h5>
              <p>Audio streaming Web application.</p>
            </div>
          </Link>
          <Link href="https://bobchat.onrender.com" className={styles.card}>
            <div className={styles.project_img_container}>
              <Image className={styles.project_img} src={bobChat} />
            </div>
            <div className={styles.project_details}>
              <h5>
                BoB<span>Chat</span>
              </h5>
              <p>Chat web app developed using socket.io</p>
            </div>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Projects;
