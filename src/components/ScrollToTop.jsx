import React, { useEffect, useState } from "react";
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import styles from './../styles/ScrollToTop.module.scss';

const ScroolToTop = () => {
  const [visible, setVisible] = useState(false);

  const ScrollButton = () => {
    const myStyles = {
      display: visible ? "flex" : "none",
      position: "fixed",
      right: "50px",
      bottom: "50px",
      zIndex: "10001",
      height: "60px",
      lineHeight: "60px",
      textAlign: "center",
      width: "60px",
      borderRadius: "50%",
      backgroundColor: "rgb(35,189,149)",
      boxShadow: "rgb(35, 189, 149, 0.3) 0px 7px 29px 0px",
      transition: "all 0.3s ease",
    };

    const toggleVisible = () => {
      const scrolled = document.documentElement.scrollTop;
      if (scrolled > 300) {
        setVisible(true);
      } else if (scrolled <= 300) {
        setVisible(false);
      }
    };

    const scrollToTop = () => {
      window.scrollTo({
        top: 0,
        behavior: "smooth",
      });
    };

    useEffect(() => {
      window.addEventListener("scroll", toggleVisible);
    }, []);

    return (
      <button onClick={scrollToTop} style={myStyles} className={styles.scroll_to_top}>
        <ArrowUpwardIcon style={{fontWeight: 600}}/>
      </button>
    );
  };
  return <ScrollButton />;
};

export default ScroolToTop;
